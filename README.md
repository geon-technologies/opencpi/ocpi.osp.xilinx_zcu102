# xilinx

## Provided 
---
The following is provided within this repo:  
- Microzed OSP zcu102 
- OSP Development Guide zcu102
- Code Blocks for each design stage of the Development Guide as a reference   

## Getting Started
---
See zcu102_Getting_Started_Guide.tex located in the doc/zcu102 directory  

## Guide for developing an OpenCPI Board Support Package (OSP) - Case Study zcu102
---
See Guide.md located in the guide/zcu102 directory  

## Version Tested against
---
This guide and subsequent OSP's were tested against and are compatable with the following OpenCPI version:  
v2.1.0 

Notes:
---
  - hdl/assemblies
      Contains copies of assemblies from the OpenCPI assets project, with the
      addition of zcu102 specific container XML files. A desirable framework
      improvement would be to mitigate the need to copy assemblies from external
      projects in order to use containers made possible internally.

